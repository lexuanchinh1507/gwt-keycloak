package com.gwt.keycloak;

public enum OnLoadAction {
  LOGIN_REQUIRED, CHECK_SSO
}
