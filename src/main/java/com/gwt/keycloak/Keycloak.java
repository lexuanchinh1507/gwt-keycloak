package com.gwt.keycloak;

import com.google.gwt.core.client.JavaScriptObject;

public class Keycloak {

    private final String key;
    private final String configURL;
    private final KeycloakImpl impl;
    private final InitOptions initOptions = new InitOptions();
    private final KeycloakListenerBroker broker = new KeycloakListenerBroker();

    /**
     * The default minimum validity time in seconds.
     * A reasonable time under which we expect a remote call to reach the server with some degree of safety.
     */
    private static final int MIN_TOKEN_VALIDITY_SECONDS = 60;

    public Keycloak(final String key, final String configURL) {
        this.key = key;
        this.configURL = configURL;
        this.impl = KeycloakImpl.create(this, configURL);
    }

    public void init() {
        init(getInitOptions());
    }

    private void init(final InitOptions options) {
        final OnLoadAction onLoadAction = options.getOnLoadAction();
        final String onLoad = OnLoadAction.LOGIN_REQUIRED == onLoadAction ? "login-required" : OnLoadAction.CHECK_SSO == onLoadAction ? "check-sso" : null;
        getImpl().init(onLoad);
    }

    protected final void onReady(final boolean authenticated) {
        getListener().onReady(this, authenticated);
    }

    protected final void onAuthSuccess() {
        getListener().onAuthSuccess(this);
    }

    protected final void onAuthError() {
        getListener().onAuthError(this);
    }

    protected final void onAuthRefreshSuccess() {
        getListener().onAuthRefreshSuccess(this);
    }

    protected final void onAuthRefreshError() {
        getImpl().clearToken();
        getListener().onAuthRefreshError(this);
    }

    protected final void beforeAuthLogout() {
        getListener().beforeAuthLogout(this);
    }

    protected final void onAuthLogout() {
        getListener().onAuthLogout(this);
    }

    protected final void onTokenExpired() {
        getListener().onTokenExpired(this);
    }


    /**
     * Redirects to login form on (options is an optional object with redirectUri and/or prompt fields).
     */
    public void login() {
        getImpl().login();
    }

    /**
     * Redirects to logout.
     */
    public void logout(String redirectUri) {
        getImpl().logout(redirectUri);
    }

    /**
     * Redirects to logout.
     */
    public void logout() {
        getImpl().logout(null);
    }


    /**
     * If the token expires within minValidity seconds the token is refreshed.
     * If the session status iframe is enabled, the session status is also checked.
     * On failure the tokens are cleared.
     */
    public void updateToken(final int minValiditySeconds, final Runnable successCallback, final Runnable failureCallback) {
        getImpl().updateToken(minValiditySeconds, successCallback, failureCallback);
    }

    /**
     * Invokes {@link #updateToken(int, Runnable, Runnable)} with null failureCallback.
     */
    public void updateToken(final int minValiditySeconds, final Runnable successCallback) {
        updateToken(minValiditySeconds, successCallback, null);
    }

    /**
     * Invokes {@link #updateToken(int, Runnable)} with {@link #MIN_TOKEN_VALIDITY_SECONDS} for minValiditySeconds.
     */
    public void updateToken(final Runnable successCallback) {
        updateToken(MIN_TOKEN_VALIDITY_SECONDS, successCallback);
    }

    /**
     * Called when token refresh succeeds.
     */
    protected final void onTokenUpdateSuccess(final Runnable listener) {
        if (null != listener) {
            listener.run();
        }
    }

    /**
     * Called when token refresh fails.
     */
    protected final void onTokenUpdateFailure(final Runnable listener) {
        getImpl().clearToken();
        if (null != listener) {
            listener.run();
        }
    }

    public String getKey() {
        return key;
    }

    public String getConfigURL() {
        return configURL;
    }

    public KeycloakImpl getImpl() {
        return impl;
    }

    public InitOptions getInitOptions() {
        return initOptions;
    }

    /**
     * Returns true if the token has less than minValiditySeconds seconds left before it expires.
     */
    public boolean isTokenExpired(final int minValiditySeconds) {
        return getImpl().isTokenExpired(minValiditySeconds);
    }

    /**
     * Returns true if the token is expired.
     */
    public boolean isTokenExpired() {
        return isTokenExpired(0);
    }

    /**
     * Is true if the user is authenticated, false otherwise.
     */
    public boolean isAuthenticated() {
        return getImpl().isAuthenticated();
    }

    public String getUsername() {
        return getImpl().getUsername();
    }

    /**
     * The base64 encoded token that can be sent in the Authorization header in requests to services.
     */
    public String getToken() {
        return getImpl().getToken();
    }

    /**
     * The parsed token as a JavaScript object.
     */
    @SuppressWarnings("unchecked")
    public <T> T getParsedToken() {
        return (T) getImpl().getParsedToken();
    }

    /**
     * The user id.
     */
    public String getSubject() {
        return getImpl().getSubject();
    }


    /**
     * The base64 encoded refresh token.
     */
    public String getRefreshToken() {
        return getImpl().getRefreshToken();
    }


    /**
     * Return the listener associated with the Keycloak instance.
     *
     * @return the listener associated with the Keycloak instance.
     */
    private KeycloakListener getListener() {
        return broker.getListener();
    }

    /**
     * Add a listener to receive messages from the Keycloak instance.
     *
     * @param listener the listener to receive messages from the Keycloak instance.
     */
    public void addKeycloakListener(final KeycloakListener listener) {
        broker.addKeycloakListener(listener);
    }

    public void removeKeycloakListener(final KeycloakListener listener) {
        broker.removeKeycloakListener(listener);
    }

    private final static class KeycloakImpl extends JavaScriptObject {
        static native KeycloakImpl create(final Keycloak keycloak, final String configURL)
            /*-{
                var impl = $wnd.Keycloak(configURL);

                impl.wrapper = keycloak;
                impl.onReady = $entry(function (authenticated) {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onReady(*)(authenticated);
                });
                impl.onAuthSuccess = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onAuthSuccess()();
                });
                impl.onAuthError = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onAuthError()();
                });
                impl.onAuthRefreshSuccess = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onAuthRefreshSuccess()();
                });
                impl.onAuthRefreshError = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onAuthRefreshError()();
                });
                impl.onAuthLogout = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onAuthLogout()();
                });
                impl.onTokenExpired = $entry(function () {
                    impl.wrapper.@com.gwt.keycloak.Keycloak::onTokenExpired()();
                });
                return impl;

            }-*/;

        protected KeycloakImpl() {
        }

        native void init(final String onLoad) /*-{
            var options = {};
            if (onLoad != null) {
                options.onLoad = onLoad;
            }
            this.init(options);
        }-*/;

        native boolean isAuthenticated() /*-{
            return this.authenticated;
        }-*/;

        native String getSubject() /*-{
            return this.subject;
        }-*/;

        native String getToken() /*-{
            return this.token;
        }-*/;

        native <T extends JavaScriptObject> T getParsedToken() /*-{
            return this.tokenParsed;
        }-*/;

        native String getRefreshToken() /*-{
            return this.refreshToken;
        }-*/;

        native boolean isTokenExpired(final int minValiditySeconds) /*-{
            return this.isTokenExpired(minValiditySeconds);
        }-*/;


        /**
         * Clear authentication state, including tokens.
         * This can be useful if application has detected the session was expired, for example if updating token fails.
         * Invoking this results in onAuthLogout callback listener being invoked.
         */
        native boolean clearToken() /*-{
            return this.clearToken();
        }-*/;

        native void logout(final String redirectUri) /*-{
            var options = {};
            if (redirectUri != null) {
                options.redirectUri = redirectUri;
            }
            this.logout(options);
        }-*/;

        native void login() /*-{
            var options = {};
            this.login(options)
        }-*/;

        native void updateToken(final int minValiditySeconds,
                                final Runnable successCallback,
                                final Runnable failureCallback) /*-{
            var keycloak = this;
            var onSuccess =
                $entry(function () {
                    keycloak.wrapper.@com.gwt.keycloak.Keycloak::onTokenUpdateSuccess(*)(successCallback);
                });
            var onFailure =
                $entry(function () {
                    keycloak.wrapper.@com.gwt.keycloak.Keycloak::onTokenUpdateFailure(*)(failureCallback);
                });
            this.updateToken(minValiditySeconds).success(onSuccess).error(onFailure);
        }-*/;

        native String getUsername() /*-{
            return this.tokenParsed ? this.tokenParsed.preferred_username : "";
        }-*/;
    }

}
