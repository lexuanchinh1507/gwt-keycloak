package com.gwt.keycloak;

/**
 * Callback invoked when keycloak initializes or fails to initialize.
 */
public interface KeycloakListener {
    /**
     * Called when the adapter is initialized.
     */
    void onReady(Keycloak keycloak, boolean isAuthenticated);

    /**
     * Called when a user is successfully authenticated.
     */
    void onAuthSuccess(Keycloak keycloak);

    /**
     * Called if there was an error during authentication.
     */
    void onAuthError(Keycloak keycloak);

    /**
     * Called when the token is refreshed.
     */
    void onAuthRefreshSuccess(Keycloak keycloak);

    /**
     * Called if there was an error while trying to refresh the token.
     */
    void onAuthRefreshError(Keycloak keycloak);

    /**
     * Called before logout action occurs.
     */
    void beforeAuthLogout(Keycloak keycloak);

    /**
     * Called if the user is logged out( will only be called if the session status iframe is enabled,
     * or in Cordova mode ).
     */
    void onAuthLogout(Keycloak keycloak);

    /**
     * Called when the access token is expired. When this happens you can for
     * refresh the token, or if refresh is not available(ie.with implicit flow ) you can redirect to login screen.
     */
    void onTokenExpired(Keycloak keycloak);
}
