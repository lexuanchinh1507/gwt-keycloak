package com.gwt.keycloak;

/**
 * Options class to pass to init action.
 */
public final class InitOptions {

    private OnLoadAction loginRequired = OnLoadAction.LOGIN_REQUIRED;
    private String token;
    private String refreshToken;
    private String idToken;
    private Integer timeSkewInSeconds;
    private boolean checkLoginIframe = true;
    private int checkLoginIframeIntervalInSeconds = 5;

    public OnLoadAction getOnLoadAction() {
        return loginRequired;
    }

    public InitOptions setOnLoadAction(final OnLoadAction onLoadAction) {
        loginRequired = onLoadAction;
        return this;
    }

    public String getToken() {
        return token;
    }

    public InitOptions setToken(final String token) {
        this.token = token;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public InitOptions setRefreshToken(final String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public String getIdToken() {
        return idToken;
    }

    public InitOptions setIdToken(final String idToken) {
        this.idToken = idToken;
        return this;
    }

    public Integer getTimeSkewInSeconds() {
        return timeSkewInSeconds;
    }

    public InitOptions setTimeSkewInSeconds(final Integer timeSkewInSeconds) {
        this.timeSkewInSeconds = timeSkewInSeconds;
        return this;
    }

    public boolean isCheckLoginIframe() {
        return checkLoginIframe;
    }

    public InitOptions setCheckLoginIframe(final boolean checkLoginIframe) {
        this.checkLoginIframe = checkLoginIframe;
        return this;
    }

    public int getCheckLoginIframeIntervalInSeconds() {
        return checkLoginIframeIntervalInSeconds;
    }

    public InitOptions setCheckLoginIframeIntervalInSeconds(final int checkLoginIframeIntervalInSeconds) {
        this.checkLoginIframeIntervalInSeconds = checkLoginIframeIntervalInSeconds;
        return this;
    }
}
