package com.gwt.keycloak;

public class KeycloakListenerAdapter implements KeycloakListener {
    @Override
    public void onReady(Keycloak keycloak, boolean isAuthenticated) {

    }

    @Override
    public void onAuthSuccess(Keycloak keycloak) {

    }

    @Override
    public void onAuthError(Keycloak keycloak) {

    }

    @Override
    public void onAuthRefreshSuccess(Keycloak keycloak) {

    }

    @Override
    public void onAuthRefreshError(Keycloak keycloak) {

    }

    @Override
    public void beforeAuthLogout(Keycloak keycloak) {

    }

    @Override
    public void onAuthLogout(Keycloak keycloak) {

    }

    @Override
    public void onTokenExpired(Keycloak keycloak) {

    }
}
