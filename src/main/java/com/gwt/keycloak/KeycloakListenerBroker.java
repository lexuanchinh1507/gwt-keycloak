package com.gwt.keycloak;

import java.util.ArrayList;
import java.util.List;

final class KeycloakListenerBroker {

    private final KeycloakListener listener = new ForwardingListener();
    private List<KeycloakListener> listeners = new ArrayList<>();

    void addKeycloakListener(final KeycloakListener listener) {
        listeners.add(listener);
    }

    void removeKeycloakListener(final KeycloakListener listener) {
        listeners.remove(listener);
    }

    public KeycloakListener getListener() {
        return listener;
    }

    private final class ForwardingListener implements KeycloakListener {

        @Override
        public void onReady(Keycloak keycloak, boolean isAuthenticated) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onReady(keycloak, isAuthenticated);
            }
        }

        @Override
        public void onAuthSuccess(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onAuthSuccess(keycloak);
            }
        }

        @Override
        public void onAuthError(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onAuthError(keycloak);
            }
        }

        @Override
        public void onAuthRefreshSuccess(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onAuthRefreshSuccess(keycloak);
            }
        }

        @Override
        public void onAuthRefreshError(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onAuthRefreshError(keycloak);
            }
        }

        @Override
        public void beforeAuthLogout(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.beforeAuthLogout(keycloak);
            }
        }

        @Override
        public void onAuthLogout(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onAuthLogout(keycloak);
            }
        }

        @Override
        public void onTokenExpired(Keycloak keycloak) {
            for (final KeycloakListener listener : new ArrayList<>(listeners)) {
                listener.onTokenExpired(keycloak);
            }
        }
    }
}
